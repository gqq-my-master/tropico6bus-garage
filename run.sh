#!/bin/bash

args=(
# Image Settings affect subsequent processing such as reading an image
-density 300 
-background white 
-delay 150  
# Define the GIF disposal image setting for images that are being created
# Somehow this option must appear before input images.
-dispose Background 
-loop 0 
garage.pdf  
# image operators affect the image immediately.
# An operator is applied to the current image set and forgotten. 

# Composite the image over the background color.
-alpha remove
-layers optimize
garage.gif
)

magick "${args[@]}"
